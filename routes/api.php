<?php

use App\Http\Controllers\API\AccountController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\TransferController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login',[AuthController::class,'login']);

Route::group(['middleware' => ['auth:sanctum']],function () {

	// create new account
	Route::post('accounts',[AccountController::class,'store'])->name('accounts.store');

	// get balance
	Route::get('accounts/{number}/balance',[AccountController::class,'getBalance'])->name('accounts.balance');

	// deposit
	Route::post('accounts/deposit',[AccountController::class,'deposit'])->name('accounts.deposit');

	// withdraw
	Route::post('accounts/withdraw',[AccountController::class,'withdraw'])->name('accounts.withdraw');


	// transfer
	Route::post('transfers',[TransferController::class,'transfer'])->name('transfers');

	// get all transfer for specific account
	Route::get('transfers/{number}',[TransferController::class,'getTransfersByAccount']);

	// API route for logout user
	Route::post('/logout',[AuthController::class,'logout']);
});