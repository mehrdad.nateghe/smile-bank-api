# Smile Bank API

✔ Create a new bank account for a customer, with an initial deposit amount. A single customer may have multiple bank accounts.

✔ Transfer amounts between any two accounts, including those owned by different customers.

✔ Retrieve balances for a given account.

✔ Retrieve transfer history for a given account

## Installation
```
git clone git@gitlab.com:mehrdad.nateghe/smile-bank-api.git
```

```
composer install
```

Create database and rename .env.example to .env and set database


```
php artisan migrate --seed
```

```
php artisan serv
```

```
php artisan test
```

## Postman Endpoints
[Share link to the collection](https://go.postman.co/workspace/My-Workspace~cbcf7266-a573-4c25-811c-daab4cf59446/collection/15392443-76112132-dd69-4fc5-9eb5-cf16906b28fa)
