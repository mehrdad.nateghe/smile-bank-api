<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		// create employee
		$user = User::create([
			'name'              => 'Good Employee',
			'email'             => 'good.employee@gmail.com',
			'email_verified_at' => Carbon::now(),
			'password'          => bcrypt('12345678'),
		]);

		$user->employee()->create();

		// create customers
		$customers = [
			'Arisha Barron', 'Branden Gibson', 'Rhonda Church', 'Georgina Hazel'
		];

		foreach ($customers as $customer){
			Customer::create([
				'name' => $customer
			]);
		}
	}
}
