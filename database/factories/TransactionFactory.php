<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
			//'type'        => 'withdraw',
			//'amount'      => 1000,
			'confirmed'   => TRUE,
			//'description' => 'desc',
        ];
    }
}
