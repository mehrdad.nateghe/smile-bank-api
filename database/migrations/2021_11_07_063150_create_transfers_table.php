<?php

use App\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();

			$table->unsignedBigInteger('from_id');
			$table->foreign('from_id')
				  ->references('id')
				  ->on('accounts')
				  ->onDelete('cascade');

			$table->unsignedBigInteger('to_id');
			$table->foreign('to_id')
				  ->references('id')
				  ->on('accounts')
				  ->onDelete('cascade');

			$table->unsignedBigInteger('deposit_id');
			$table->foreign('deposit_id')
				  ->references('id')
				  ->on('transactions')
				  ->onDelete('cascade');

			$table->unsignedBigInteger('withdraw_id');
			$table->foreign('withdraw_id')
				  ->references('id')
				  ->on('transactions')
				  ->onDelete('cascade');

			$table->decimal('amount',64,0);

			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
