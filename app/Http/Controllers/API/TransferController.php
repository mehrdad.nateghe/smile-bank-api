<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Transfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransferController extends Controller
{
	public function transfer(Request $request)
	{
		$this->validate($request,[
			'from'   => 'required|exists:accounts,number|different:to',
			/*'from'   => ['required','exists:accounts,number','different:to',function ($attribute,$value,$fail) use ($request) {
				if($value === $request->to){
					$fail("It is not possible, You can not transfer to a customer's accounts!");
				}
			}],*/
			'to'     => 'required|exists:accounts,number',
			'amount' => 'required',
		]);

		$hasEnoughBalance = Account::query()->where('number',$request->from)->where('balance','>=',$request->amount)->first();

		if(empty($hasEnoughBalance)){
			return response()->json([
				'status'  => 'error',
				'message' => 'The source account does not have enough balance!'
			]);
		}

		DB::transaction(function () use ($request) {
			$from = Account::query()->where('number',$request->from)->where('balance','>=',$request->amount)->lockForUpdate()->first();
			$to   = Account::query()->where('number',$request->to)->lockForUpdate()->first();

			$withdraw = $from->transactions()->create([
				'type'        => 'withdraw',
				'amount'      => $request->amount,
				'confirmed'   => TRUE,
				'description' => 'Transfer to ' . $to->number,
			]);

			$deposit = $to->transactions()->create([
				'type'        => 'deposit',
				'amount'      => $request->amount,
				'confirmed'   => TRUE,
				'description' => 'Transfer from ' . $from->number,
			]);

			Transfer::create([
				'from_id'     => $from->id,
				'to_id'       => $to->id,
				'withdraw_id' => $withdraw->id,
				'deposit_id'  => $deposit->id,
				'amount'      => $request->amount,
			]);

			$from->decrement('balance',$request->amount);
			$to->increment('balance',$request->amount);
		});

		return response()->json([
			'status'  => 'success',
		]);
	}

	public function getTransfersByAccount($accountNumber)
	{
		$account = Account::where('number', $accountNumber)->firstOrFail();
		$data = [];
		foreach ($account->transfers as $transfer) {
			array_push($data,[
				'from'   => $transfer->from->customer->name,
				'to'     => $transfer->to->customer->name,
				'amount' => $transfer->amount,
			]);
		}

		return response()->json([
			'status'  => 'success',
			'data'    => $data,
		]);
	}
}
