<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
	public function login(Request $request)
	{
		$this->validate($request,[
			'email'    => 'required|email',
			'password' => 'required',
		]);


		if(!Auth::attempt($request->only('email','password'))){
			//dd($request->email,$request->password);
			return response()->json([
				'message' => 'Unauthorized'
			],Response::HTTP_UNAUTHORIZED);
		}

		$user = User::where('email',$request['email'])->firstOrFail();

		$token = $user->createToken('auth_token')->plainTextToken;

		return response()->json([
			'message'      => 'Hi ' . $user->name . ', welcome to Smile Bank',
			'access_token' => $token,
			'token_type'   => 'Bearer',
		]);
	}

	// method for user logout and delete token
	public function logout()
	{
		auth()->user()->tokens()->delete();

		return [
			'message' => 'You have successfully logged out and the token was successfully deleted'
		];
	}
}
