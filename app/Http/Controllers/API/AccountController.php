<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
	/**
	 * @OA\Post(
	 *      path="/api/accounts",
	 *      operationId="store",
	 *      tags={"Create new accounts"},
	 *      summary="Create new accounts",
	 *      description="Create new accounts by employee",
	 *      @OA\Response(
	 *          response=200,
	 *          description="successful operation"
	 *       )
	 *     )
	 *
	 * Returns customer and list of accounts
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'customer_name'              => 'required|regex:/^[\pL\s\/]+$/u',
			'accounts.*.number'          => 'required|digits:9|unique:accounts,number',
			'accounts.*.initial_deposit' => 'required|numeric|min:1000',
		]);

		$customer = Customer::create([
			'name' => $request->customer_name
		]);

		$accounts = [];
		foreach ($request->accounts as $account) {
			$newAccount = $customer->accounts()->create([
				'number'  => $account['number'],
				'balance' => $account['initial_deposit'],
			]);

			$transaction = $newAccount->transactions()->create([
				'type'        => 'deposit',
				'amount'      => $account['initial_deposit'],
				'confirmed'   => TRUE,
				'description' => 'Create new account by ' . Auth::user()->name . ' for customer ' . $request->customer_name,
			]);

			array_push($accounts,[
				'account'     => $newAccount,
				'transaction' => $transaction,
			]);
		}

		return response()->json([
			'status' => 'success',
			'data'   => [
				'customer' => $customer,
				'accounts' => $accounts,
			]
		]);
	}

	public function deposit(Request $request)
	{
		$this->validate($request,[
			'account_number' => 'required|digits:9|exists:accounts,number',
			'amount'         => 'required|numeric|min:1000',
		]);

		DB::transaction(function () use ($request) {
			$account = Account::query()->where('number',$request->account_number)->lockForUpdate()->first();

			$account->transactions()->create([
				'type'        => 'deposit',
				'amount'      => $request->amount,
				'confirmed'   => TRUE,
				'description' => 'Deposit',
			]);

			$account->increment('balance',$request->amount);
		});

		return response()->json([
			'status' => 'success',
			'data'   => [
				'amount' => $request->amount,
			]
		]);
	}




	public function withdraw(Request $request)
	{
		$this->validate($request,[
			'account_number' => 'required|digits:9|exists:accounts,number',
			'amount'         => 'required|numeric|min:1000',
		]);

		$hasEnoughBalance = Account::query()->where('number',$request->account_number)->where('balance','>=',$request->amount)->first();

		if(empty($hasEnoughBalance)){
			return response()->json([
				'status'  => 'error',
				'data'    => NULL,
				'message' => 'The source account does not have enough balance!'
			]);
		}

		DB::transaction(function () use ($request) {
			$account = Account::query()->where('number',$request->account_number)->where('balance','>=',$request->amount)->lockForUpdate()->first();

			$account->transactions()->create([
				'type'        => 'withdraw',
				'amount'      => $request->amount,
				'confirmed'   => TRUE,
				'description' => 'withdraw',
			]);

			$account->decrement('balance',$request->amount);
		});

		return response()->json([
			'status' => 'success',
			'data'   => [
				'amount' => $request->amount,
			]
		]);
	}

	public function getBalance($accountNumber)
	{
		$account = Account::query()->where('number', $accountNumber)->firstOrFail();
		return response()->json([
			'status' => 'success',
			'data'   => [
				'balance' => $account->balance
			]
		]);
	}
}
