<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = ['number','balance'];

	public function customer()
	{
		return $this->belongsTo(Customer::class);
    }

	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}

	public function transfers()
	{
		return $this->hasMany(Transfer::class, 'from_id', 'id')->orWhere('to_id', 'id');
	}
}
