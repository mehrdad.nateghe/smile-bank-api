<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    use HasFactory;

    protected $fillable = ['from_id','to_id','withdraw_id','deposit_id','amount'];

	public function from()
	{
		return $this->belongsTo(Account::class,'from_id','id');
	}

	public function to()
	{
		return $this->belongsTo(Account::class,'to_id','id');
	}

	public function deposit()
	{
		return $this->belongsTo(Transaction::class, 'deposit_id','id');
	}

	public function withdraw()
	{
		return $this->belongsTo(Transaction::class, 'withdraw_id','id');
	}
}
