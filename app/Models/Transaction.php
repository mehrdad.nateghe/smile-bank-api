<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	use HasFactory;

	protected $fillable = [
		'type',
		'amount',
		'confirmed',
		'description',
	];

	public function account()
	{
		return $this->belongsTo(Account::class);
	}

	public function transferDeposit()
	{
		return $this->hasMany(Transfer::class,'deposit_id','id');
	}

	public function transferWithdraw()
	{
		return $this->hasMany(Transfer::class,'withdraw_id','id');
	}


}
