<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\Transaction;
use Database\Factories\CustomerFactory;
use Database\Factories\UserFactory;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AccountTest extends TestCase
{
	use DatabaseTransactions;

	/**
	 * A basic feature test example.
	 *
	 * @return void
	 */
	public function test_create_account()
	{
		$user = UserFactory::new()->create();

		$faker = Factory::create();

		$customerName      = $faker->firstName() . ' ' . $faker->lastName();
		$numberOne         = $faker->numberBetween(100000000,999999999);
		$numberTwo         = $faker->numberBetween(100000000,999999999);
		$initialDepositOne = $faker->numberBetween(1000,9000);
		$initialDepositTwo = $faker->numberBetween(1000,9000);

		$response = $this->actingAs($user)
						 ->withHeaders([
							 'Content-Type' => 'application/json',
							 'Accept'       => 'application/json',
						 ])->json('post',route('accounts.store'),[
				'customer_name' => $customerName,
				'accounts'      => [
					[
						'number'          => $numberOne,
						'initial_deposit' => $initialDepositOne,
					],
					[
						'number'          => $numberTwo,
						'initial_deposit' => $initialDepositTwo,
					]
				],
			]);

		$this->assertDatabaseHas('customers',[
			'name' => $customerName
		]);

		$this->assertDatabaseHas('accounts',[
			'number'  => $numberOne,
			'balance' => $initialDepositOne,
		]);

		$this->assertDatabaseHas('accounts',[
			'number'  => $numberTwo,
			'balance' => $initialDepositTwo,
		]);

		$this->assertDatabaseHas('transactions',[
			'type'      => 'deposit',
			'amount'    => $initialDepositOne,
			'confirmed' => TRUE,
		]);

		$this->assertDatabaseHas('transactions',[
			'type'      => 'deposit',
			'amount'    => $initialDepositTwo,
			'confirmed' => TRUE,
		]);

		$response->assertJsonStructure([
			'status',
			'data' => [
				'customer',
				'accounts'
			]
		]);

		$response->assertStatus(200);
	}

	public function test_deposit()
	{
		$user     = UserFactory::new()->create();
		$customer = CustomerFactory::new()->create();
		$account  = Account::factory()
						   ->count(1)
						   ->for($customer)
						   ->create();

		$faker = Factory::create();

		$depositAmount = $faker->numberBetween(1000,9000);

		$response = $this->actingAs($user)
						 ->withHeaders([
							 'Content-Type' => 'application/json',
							 'Accept'       => 'application/json',
						 ])->json('post',route('accounts.deposit'),[
				'account_number' => $account[0]['number'],
				'amount'         => $depositAmount,
			]);

		$response->assertStatus(200);

		$response->assertJsonFragment([
			'amount' => $depositAmount
		]);
	}

	public function test_withdraw()
	{
		$user     = UserFactory::new()->create();
		$customer = CustomerFactory::new()->create();
		$account  = Account::factory()
						   ->count(1)
						   ->for($customer)
						   ->create();

		$faker = Factory::create();

		$depositAmount = $faker->numberBetween(5000,9000);

		$responseDeposit = $this->actingAs($user)
						 ->withHeaders([
							 'Content-Type' => 'application/json',
							 'Accept'       => 'application/json',
						 ])->json('post',route('accounts.deposit'),[
				'account_number' => $account[0]['number'],
				'amount'         => $depositAmount,
			]);

		$responseDeposit->assertStatus(200);

		$responseDeposit->assertJsonFragment([
			'amount' => $depositAmount
		]);

		$withdrawAmount = $faker->numberBetween(1000,5000);
		$responseWithdraw = $this->actingAs($user)
								->withHeaders([
									'Content-Type' => 'application/json',
									'Accept'       => 'application/json',
								])->json('post',route('accounts.withdraw'),[
				'account_number' => $account[0]['number'],
				'amount'         => $withdrawAmount,
			]);

		$responseWithdraw->assertStatus(200);
		$responseWithdraw->assertJsonFragment([
			'amount' => $withdrawAmount
		]);
	}

	public function test_get_balance()
	{
		$user     = UserFactory::new()->create();
		$customer = CustomerFactory::new()->create();
		$account  = Account::factory()
						   ->count(1)
						   ->for($customer)
						   ->create();

		$faker = Factory::create();

		// deposit
		$depositAmount = $faker->numberBetween(5000,9000);
		$responseDeposit = $this->actingAs($user)
								->withHeaders([
									'Content-Type' => 'application/json',
									'Accept'       => 'application/json',
								])->json('post',route('accounts.deposit'),[
				'account_number' => $account[0]['number'],
				'amount'         => $depositAmount,
			]);
		$responseDeposit->assertStatus(200);
		$responseDeposit->assertJsonFragment([
			'amount' => $depositAmount
		]);


		// withdraw
		$withdrawAmount = $faker->numberBetween(1000,5000);
		$responseWithdraw = $this->actingAs($user)
								 ->withHeaders([
									 'Content-Type' => 'application/json',
									 'Accept'       => 'application/json',
								 ])->json('post',route('accounts.withdraw'),[
				'account_number' => $account[0]['number'],
				'amount'         => $withdrawAmount,
			]);
		$responseWithdraw->assertStatus(200);
		$responseWithdraw->assertJsonFragment([
			'amount' => $withdrawAmount
		]);

		// get balance
		$responseGetBalance = $this->actingAs($user)
								 ->withHeaders([
									 'Content-Type' => 'application/json',
									 'Accept'       => 'application/json',
								 ])->json('get',route('accounts.balance', $account[0]['number']));

		$responseGetBalance->assertStatus(200);
		$balance = $depositAmount - $withdrawAmount;
		$responseGetBalance->assertJsonFragment([
			'balance' => (string) $balance
		]);
	}
}




