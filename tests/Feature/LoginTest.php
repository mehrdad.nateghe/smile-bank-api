<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class LoginTest extends TestCase
{
	use DatabaseTransactions;

	/**
	 * A basic feature test example.
	 *
	 * @return void
	 */
	public function test_login_with_wrong_data()
	{
		$response = $this->withHeaders([
			'Content-Type' => 'application/json',
			'Accept'       => 'application/json',
		])->json('post','api/login',[
			'email'    => 'badUsername@gmail.com',
			'password' => 'badPass',
		]);

		$response->assertStatus(401);
	}

	/**
	 * A basic feature test example.
	 *
	 * @return void
	 */
	public function test_login_with_correct_data()
	{
		$response = $this->withHeaders([
			'Content-Type' => 'application/json',
			'Accept'       => 'application/json',
		])->json('post','api/login',[
			'email'    => 'good.employee@gmail.com',
			'password' => '12345678',
		]);

		$response->assertStatus(200);
		$response->assertJsonStructure(['access_token']);
	}
}
