<?php

namespace Tests\Feature;

use App\Models\Account;
use Database\Factories\CustomerFactory;
use Database\Factories\UserFactory;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransferTest extends TestCase
{
	use DatabaseTransactions;

	private $accountOne;

	public function test_get_transfers_by_account()
	{
		$this->test_transfer();
		$user = UserFactory::new()->create();

		// get transfer by account
		$response = $this->actingAs($user)
						 ->withHeaders([
							 'Content-Type' => 'application/json',
							 'Accept'       => 'application/json',
						 ])->json('get',route('accounts.balance',$this->accountOne[0]['number']));

		$response->assertStatus(200);

	}

	/**
	 * A basic feature test example.
	 *
	 * @return void
	 */
	public function test_transfer()
	{
		$faker = Factory::create();
		$user  = UserFactory::new()->create();

		// user one
		$customerOne      = CustomerFactory::new()->create();
		$this->accountOne = Account::factory()
								   ->count(1)
								   ->for($customerOne)
								   ->create();

		$depositAmountForUserOne   = '5000';
		$responseDepositForUserOne = $this->actingAs($user)
										  ->withHeaders([
											  'Content-Type' => 'application/json',
											  'Accept'       => 'application/json',
										  ])->json('post',route('accounts.deposit'),[
				'account_number' => $this->accountOne[0]['number'],
				'amount'         => $depositAmountForUserOne,
			]);

		$responseDepositForUserOne->assertStatus(200);

		// user two
		$customerTwo = CustomerFactory::new()->create();
		$accountTwo  = Account::factory()
							  ->count(1)
							  ->for($customerTwo)
							  ->create();

		$depositAmountForUserTwo   = '2000';
		$responseDepositForUserTwo = $this->actingAs($user)
										  ->withHeaders([
											  'Content-Type' => 'application/json',
											  'Accept'       => 'application/json',
										  ])->json('post',route('accounts.deposit'),[
				'account_number' => $accountTwo[0]['number'],
				'amount'         => $depositAmountForUserTwo,
			]);


		$responseDepositForUserTwo->assertStatus(200);

		// transfer
		$responseTransfer = $this->actingAs($user)
								 ->withHeaders([
									 'Content-Type' => 'application/json',
									 'Accept'       => 'application/json',
								 ])->json('post',route('transfers'),[
				'from'   => $this->accountOne[0]['number'],
				'to'     => $accountTwo[0]['number'],
				'amount' => '2000',
			]);

		$responseTransfer->assertStatus(200);
		$responseTransfer->assertJsonFragment([
			'status' => 'success'
		]);

		// get balance account one
		$responseGetBalanceAccountOne = $this->actingAs($user)
											 ->withHeaders([
												 'Content-Type' => 'application/json',
												 'Accept'       => 'application/json',
											 ])->json('get',route('accounts.balance',$this->accountOne[0]['number']));

		$responseGetBalanceAccountOne->assertStatus(200);
		$responseGetBalanceAccountOne->assertJsonFragment([
			'balance' => "3000"
		]);

		// get balance account two
		$responseGetBalanceAccountTwo = $this->actingAs($user)
											 ->withHeaders([
												 'Content-Type' => 'application/json',
												 'Accept'       => 'application/json',
											 ])->json('get',route('accounts.balance',$accountTwo[0]['number']));

		$responseGetBalanceAccountTwo->assertStatus(200);
		$responseGetBalanceAccountTwo->assertJsonFragment([
			'balance' => "4000"
		]);
	}
}
